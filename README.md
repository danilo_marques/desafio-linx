# Desafios

Esse é um desafio desenvolvido como teste para a vaga de desenvolvedor backend.
Esse desafio é composto por dois testes:

## part-1

Desenvolver uma API que receberá requisições no formato JSON com informações de produtos. Caso uma requisição com o mesmo corpo seja enviado em menos de 10 minutos ela deve ser recusada.

## part-2

Criar um agregador de URLs capaz de ler uma lista de URLs de imagens, sanitaniza-las e devolver em um formato agrupado pelo productId.
