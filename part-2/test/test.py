import json
import sys
import unittest

sys.path.insert(0, '../src')

import aggregator

class TestAggregator(unittest.TestCase):

    def test_format_product_should_return_formated_product(self):

        aggregator_format = aggregator.format_product('pid123', ['image1','image2','image3'])

        expected_data = {
            'productId': 'pid123',
            'images': ['image1','image2','image3']
        }

        self.assertEqual(expected_data, aggregator_format)

    def test_validate_image_should_ignore_invalid_images(self):

        mock_product = {
            'productId': 'pid123',
            'images': [
                'http://localhost:4567/images/5.png',
                'http://localhost:4567/images/4.png', # should be ignored. (4 % 5 != 0)
                'http://localhost:4567/images/50.png',
                'http://localhost:4567/images/6.png', # should be ignored. (6 % 5 != 0)
            ]
        }

        validated_images = aggregator.validate_image(mock_product)

        expected_data = {
            'productId': 'pid123',
            'images': [
                'http://localhost:4567/images/5.png',
                'http://localhost:4567/images/50.png',
            ]
        }

        self.assertEqual(expected_data, validated_images)

    def test_validate_image_should_limit_amount_of_images_to_three(self):

        mock_product = {
            'productId': 'pid123',
            'images': [
                'http://localhost:4567/images/5.png',
                'http://localhost:4567/images/12.png', # should be ignored. (12 % 5 != 0)
                'http://localhost:4567/images/50.png',
                'http://localhost:4567/images/500.png',
                'http://localhost:4567/images/5000.png',
                'http://localhost:4567/images/50000.png'
            ]
        }

        validated_images = aggregator.validate_image(mock_product)

        expected_data = {
            'productId': 'pid123',
            'images': [
                'http://localhost:4567/images/5.png',
                'http://localhost:4567/images/50.png',
                'http://localhost:4567/images/500.png'
            ]
        }

        self.assertEqual(expected_data, validated_images)

if __name__ == '__main__':
    unittest.main()
