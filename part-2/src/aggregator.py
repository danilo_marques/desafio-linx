import concurrent.futures
import json
import os
import tarfile

from urllib.parse import urlparse

# Aggregates products
def aggregate(dump):
    
    # Extracts file from tar.gz
    tar = tarfile.open(dump, "r:gz")
    tar.extractall()
    tar.close()

    products = {}
    for line in open('input-dump'):

        product = json.loads(line)
        productId = product.get('productId')

        if products.get(productId):
            products[productId].append(product.get('image'))
        else:
            products[productId] = [product.get('image')]

    final_dump = open('./final-dump.json', 'w+')

    for product in map(format_product, products.keys(), products.values()):
        validated_image = validate_image(product)

        if not validated_image.get("images"):
            continue

        final_dump.write(json.dumps(validated_image) + '\n')

    os.remove('input-dump')

    print('Execucao do programa concluida!')

# Formats products to format: {productId: pid123 images: [image1], [image2], ...}
def format_product(productId, images):
    return {
        'productId': productId,
        'images': images
    }

# Checks if the images from the product are valid
def validate_image(product):

    existing_images = []
    for image in product.get('images'):

        parsed_image = urlparse(image)
        image_name = os.path.basename(parsed_image.path).split(".")[0]
        
        image_res_mod = int(image_name) % 5

        if (image_res_mod == 0):
            existing_images.append(image)
        if len(existing_images) >= 3:
            break
    
    product['images'] = existing_images
    return product