import aggregator
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('dump', help='Entre com o caminho do arquivo dump')
args = parser.parse_args()

aggregator.aggregate(args.dump)