# Desafio Agregador de Urls

A ideia desse desafio é desenvolver um agregador de URLs capaz de ler uma lista de URLs de imagens e sanitaniza-las.
O comportamento dele deve ser o seguinte:

- A cada 10 minutos recebemos uma dump com uma lista de produtos. Essa dump tem imagens que retornam erro 404 que devem ser removidas e as restantes deve ser agrupadas pelo productId.

- O limite de imagens por produto deve ser de no máximo 3 imagens.

- A dump final deve ser gerada no menor tempo possível.

## Pré requisitos

[Python 3.x](https://www.python.org/downloads/release/python-390/ "Python 3.x")

## Como rodar o projeto

Para executar o projeto é bem simples, basta abrir o terminal (shell) dentro da pasta do projeto (`part-2/src`) e executar o comando abaixo:

```
python3 init.py input-dump.tar.gz
```

## Como rodar os teste unitários

Para rodar os testes unitários execute o seguite comando no terminal (shell) dentro da pasta de testes do projeto (`part-2/test`):

```
python3 test.py
```
