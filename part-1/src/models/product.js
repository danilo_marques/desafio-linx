const mongoose = require('mongoose');

const Product = new mongoose.Schema({
    id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
});

mongoose.model('product', Product)