const mongoose = require('mongoose');

const ProductRequest = new mongoose.Schema({
    hash: {
        type: String,
        required: true
    },
    created_at: {
        type : Date,
        default: Date.now
    }
});

mongoose.model('productRequest', ProductRequest);