const mongoose = require('mongoose');
const hash = require('object-hash');

// Models
require('../models/product');
const ProductModel = mongoose.model('product');

// Controllers
const ProductRequestController = require('./productRequest');

/**
 * Creates new entries at products table
 * 
 * @param request 
 * @param response 
 */
async function create (request, response) {
    const md5Hash = hash(request.body, {algorithm: 'md5'});

    if (await ProductRequestController.allowProductRequest(md5Hash)) {
        
        // Creates new product request
        ProductRequestController.create(md5Hash);

        // Creates new product
        const product = ProductModel.collection.insertMany(request.body, (err) => {
            if (err) return response.send(400, {
                message: 'Erro ao salvar o(s) produto(s)'
            })
        
            return response.send(200, {
                message: 'Produto(s) salvo(s) com sucesso!'
            })
        });

    } else {
        // REFUSE: Same request received less than 10 minutes ago
        return response.send(403, {message: 'Produto(s) atualizado(s) a menos de 10 minutos'})
    }
}

module.exports = {
    create: create
}