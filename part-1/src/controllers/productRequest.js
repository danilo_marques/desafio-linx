const mongoose = require('mongoose');

require('../models/productRequest');
const ProductRequestModel = mongoose.model('productRequest');

/**
 * Saves a new product request hash into DB
 * 
 * @param md5Hash 
 */
function create (md5Hash) {
    ProductRequestModel.create({ hash: md5Hash });
    return true;
}

/**
 * Checks if the same request was already received
 * 
 * @param md5Hash 
 */
async function allowProductRequest(md5Hash) {
    const productRequest = await ProductRequestModel.find({ hash: md5Hash})
        .sort({'created_at': -1})
        .limit(1);

    if (productRequest.length > 0 && dateTimeDiff(productRequest[0].created_at) < 10) {
        return false;
    }
    
    return true;
}

/**
 * Compare a custom date time with the currente date and return the difference in minutes
 * 
 * @param customDateTime 
 */
function dateTimeDiff(customDateTime) {
    const dateDiff = Date.now() - customDateTime;
    const minutes = dateDiff / 60000; // Milliseconds transformed into minutes

    return Math.floor(minutes);
}

module.exports = {
    create: create,
    allowProductRequest: allowProductRequest,
    dateTimeDiff: dateTimeDiff
}