const ProductRequestController = require('../src/controllers/productRequest');

describe('Product request controller test', () => {
    test('Difference between current date time and the request date time must be 8 minutes', () => {
        const requestDateTime = new Date();
        requestDateTime.setMinutes(requestDateTime.getMinutes() - 8); // Removed 8 minutes from current date time

        const dateDiff = ProductRequestController.dateTimeDiff(requestDateTime);

        expect(dateDiff).toBe(8);
    })
})