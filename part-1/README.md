# Desafio API de Produtos

A ideia desse desafio é desenvolver uma API que ao receber uma requisição no formato JSON e tem o seguinte comportamento:

- Se for uma requisição que nunca foi recebida pela API ou que já foi recebida porém a mais de 10 minutos, então os dados são salvos no banco de dados.

- Se a requisição está duplicada (a mesma requisição foi enviada a menos de 10 minutos), então a API rejeira ela.

## Pré requisitos

[Docker](https://docs.docker.com/install/ "Docker")
[Docker Compose](https://docs.docker.com/compose/install/, "Docker Compose")
[MongoDB](https://www.mongodb.com/try/download/community, "MongoDB")

## Como rodar o projeto

Primeiramente, remoneie o arquivo `.env.sample` para `.env` e coloque suas configurações de server e banco de dados.

Agora, execute o seguinte comando no terminal (shell), dentro do diretório `part-1`, para instalar as dependências do projeto:

```
npm install
```

Agora vamos subir os containers do docker. Para isso execute o seguinte comando:

```
make up
```

Depois que os containers forem iniciados execute seguinte comando para iniciar a no modo dev:

```
npm run dev
```

ou para iniciar a aplicação em modo produção execute:

```
npm run start
```

## Como rodar os teste unitários

Para rodar os testes unitários do Jest, de dentro do diretório `part-1` execute o seguite comando:

```
npm run test
```
