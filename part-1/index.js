require('./src/setup/db')

const { server } = require('./src/setup/server');

const ProductController = require('./src/controllers/product');

server.get('/', (request, response) => {
  response.send(200, { message: "Bem vindo à API Linx" })
})

server.post('/v1/products', (request, response) => {
  ProductController.create(request, response);
});

server.start(() => console.log('Server iniciado'))